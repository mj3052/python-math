import sys
import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# From panda import *
#                                       .,;;;;;;;,.
#                                     ,;;;;;;;,/;;;;
#                    .,aa###########@a;;;;;/;;;,//;;;
#           ..,,,.,aa##################@a;//;;;,//;;;
#        ,;;;;;;;O#####OO##############OOO###a,/;;;;'
#      .;;//,;;;O####OOO##########OOO####OOO#####a'
#     .;;/,;;/;OO##OO#######################OOO####.
#     ;;;/,;;//OO#######OOO###########OOO###########.
#     `;;//,;,OOO#########OO#########OO##############.
#   ;.  ``````OOO#####;;;;;;OO#####OO;;;;;;######O####.
#  .;;,       OOO###O;;' ~`;##OOOOO##;' ~`;;O#####OO###
#  ;;;;    ,  OOO##O;;;,.,;O#########O;,.,;;;O####OO###,
#  `;;'   ,;; OOO##OO;;;;OOO(???????)OOO;;;;OO####OO###%,
#    `\   ;;; `OOO#####OOOO##\?????/##OOOO#######O####%O@a
#       \,`;'  `OOO####OOO######;######OOO###########%O###,
#       .,\      `OO####OO"#####;#####"OO##########%oO###O#;
#     ,;;;; \   .::::OO##OOOaaa###aaaOOO#######',;OO##OOO##;,
#    .;;''    \:::.OOaa`###OO#######OO###'::aOO.:;;OO###OO;::.
#    '       .::\.OO####O#::;;;;;;;;;;;;::O#O@OO.::::::::://::
#           .:::.O\########O#O::;;;::O#OO#O###@OO.:;;;;;;;;//:,
#          .:/;:.OO#\#########OO#OO#OO########@OO.:;;;;;;;;;//:
#         .://;;.OO###\##########O#############@OO.:;;;;;;;;//:.
#         ;//;;;;.O'//;;;;;;\##################@OO.:;;;;;;;;//:..
#        ;//:;;;;:.//;;;;;;;;;#################@OO.:;;;;;;;;;//..
#        ;;//:;;;:://;;;;;;;;;################@OO.:/;;;;;;;;;//..
#        `;;;;;:::::::ooOOOoo#\############@OOO.;;//;;;;;;;;;//.o,
#        .;,,,.OOOOO############\#######@OOO.;;;//;;;;;;;;;;//;.OO,
#       //;;.oO##################@\OOO.;;;;;;;;;;;;;;;;;;;;//;.oO#O,
#      //;;;;O##############@OOO=;;;;//;;;;;;;;;;;;;;;;;;;//;.oO##Oo
#      //::;;O#########@OOOOO=;;;;;;;//;;;;;;;;;;;;;;;////;.oO####OO
#  .n.n.n.n`;O########@OOOOO=;;;;;;;;;;///;;;;////////';oO########OO
# .%%%%%%%%%,;;########@=;;;;=;;;;///////////////':::::::::.a######@
# /%%%%%%%%%%.;;;;""""=:://:::::::::::::::::\::::::::::::://:.####@'
# /%%%%%%%%%//.;'     =:://:::::::::::::::::::\::::::::::://:.###@'
#  /%%%%%%%%//'        =:://::::::::;:::::::::::\:::::::://:.##@'
#   /%%%%%%/             =:://:::;;:::::::::::::::\::::::::'
#     ''''                 ''''''   ''''''''''''''''\''''

def findGraphSizeWithTwoVectors(vectorA, vectorB):
	x = float()
	y = float()
	if vectorA[0] > vectorB[0]:
		x = [vectorB[0]-5, vectorA[0]+5]
	elif vectorB[0] > vectorA[0]:
		x = [vectorA[0]-5, vectorB[0]+5]
	else:
		x = [vectorA[0]-5, vectorA[0]+5]

	if vectorA[1] > vectorB[1]:
		y = [vectorB[1]-5, vectorA[1]+5]
	elif vectorB[1] > vectorA[1]:
		y = [vectorA[1]-5, vectorB[1]+5]
	else:
		y = [vectorA[1]-5, vectorA[1]+5]

	return [x,y]

def findBestGraphSize(vectors):
	largestx = 0
	smallestx = 0

	for x in range(len(vectors)):
		if vectors[x][0] > largestx:
			largestx = vectors[x][0]
		elif vectors[x][0] < smallestx:
			smallestx = vectors[x][0]

	largesty = 0
	smallesty = 0

	for x in range(len(vectors)):
		if vectors[x][1] > largesty:
		 largesty = vectors[x][1]
		elif vectors[x][1] < smallesty:
			smallesty = vectors[x][1]

	return [ [largestx, smallestx], [largesty, smallesty] ]


def drawVector(vector):
	soa =np.array( [ [0,0,vector[0],vector[1]] ]) 
	X,Y,U,V = zip(*soa)
	fig = plt.figure()
	fig.suptitle('Length of vector: ' + str("%.3f" % findLength(vector)), fontsize=14, fontweight='bold')
	ax = plt.gca()
	ax.quiver(X,Y,U,V,angles='xy',scale_units='xy',scale=1)
	ax.set_xlim([vector[0]-5,vector[0]+5])
	ax.set_ylim([vector[1]-5, vector[1]+5])
	plt.grid()
	print("Unit: " + str(findUnitVector(vector)))
	plt.show()

def solveEquation(vectorA, vectorB, angle):

	#result = math.degrees(math.acos(cosinus))
	if vectorB[0] == "x":
		a = vectorA[0]
		b = vectorA[1]
		c = vectorB[1]
		v = angle
		v2Radians = math.radians(v * 2)

		top = (2 * a * b * c) - math.sqrt((a**4 * c**2 * math.sin(v2Radians)**2 + 2 * a**2 * b**2 * c**2 * math.sin(v2Radians)**2 + b**4 * c**2 * math.sin(v2Radians)**2))

		bottom = (a**2 * math.cos(v2Radians)) - a**2 + (b**2 * math.cos(v2Radians)) + (b**2)

		x = top / bottom
		print("x = " + str(x))

def drawListOfVectors(vectors):
	vectorList = []
	for x in range(len(vectors)):
		newList = [0,0]
		newList.append(vectors[x][0])
		newList.append(vectors[x][1])
		print("Length of vector" + str(x) + " " + str(findLength(vectors[x])))
		print("Unit of vector" + str(x) + " " + str(findUnitVector(vectors[x])))
		print("\n")
		vectorList.append(newList)
	print(str(vectorList))
	soa = vectorList

	X,Y,U,V = zip(*soa)
	fig = plt.figure()
	#fig.suptitle('Length of vector: ' + str("%.3f" % findLength(vector)), fontsize=14, fontweight='bold')
	ax = plt.gca()
	ax.quiver(X,Y,U,V,angles='xy',scale_units='xy',scale=1)
	size = findBestGraphSize(vectors)

	ax.set_xlim(size[0][1]-5,size[0][0]+5)
	ax.set_ylim(size[1][1]-5,size[1][0]+5)
	plt.grid()
	plt.show()

def drawListOfVectorsConnected(vectors):
	vectorList = []
	pointSum = [0,0]
	newList = []
	for x in range(len(vectors)):
		if x == 0:
			newList = [0,0]
			newPointSum = [pointSum[0]+vectors[x][0],pointSum[1]+vectors[x][1]]
			pointSum = newPointSum
		if x != 0:
			newList = pointSum
			newPointSum = [pointSum[0]+vectors[x][0],pointSum[1]+vectors[x][1]]
			pointSum = newPointSum

		newList.append(vectors[x][0])
		newList.append(vectors[x][1])
		print("Length of vector" + str(x) + " " + str(findLength(vectors[x])))
		print("Unit of vector" + str(x) + " " + str(findUnitVector(vectors[x])))
		print("\n")
		vectorList.append(newList)
	print(str(vectorList))
	soa = vectorList

	X,Y,U,V = zip(*soa)
	fig = plt.figure()
	#fig.suptitle('Length of vector: ' + str("%.3f" % findLength(vector)), fontsize=14, fontweight='bold')
	ax = plt.gca()
	ax.quiver(X,Y,U,V,angles='xy',scale_units='xy',scale=1)
	ax.set_xlim(-1,10)
	ax.set_ylim(-1,10)
	plt.grid()
	plt.show()

def drawTwoVectorsWithAngle(vectorA,vectorB):
	angle = findAngle(vectorA,vectorB)

	soa =np.array( [ [0,0,vectorA[0],vectorA[1]], [0,0,vectorB[0],vectorB[1]] ]) 
	X,Y,U,V = zip(*soa)
	fig = plt.figure()
	fig.suptitle('Angle between vectors: ' + str("%.3f" % angle), fontsize=14, fontweight='bold')
	ax = plt.gca()
	ax.quiver(X,Y,U,V,angles='xy',scale_units='xy',scale=1)
	limits = findGraphSizeWithTwoVectors(vectorA,vectorB)
	ax.set_xlim(limits[0])
	ax.set_ylim(limits[1])
	plt.grid()
	plt.show()


def findLength(vector):
	return math.sqrt(vector[0]**2 + vector[1]**2)

def scalarProduct(vectorA, vectorB):
	return (vectorA[0] * vectorB[0]) + (vectorA[1] * vectorB[1])

def productWithNumber(number, vector):
	x = float(vector[0])
	y = float(vector[1])
	x1 = x * number
	y1 = y * number
	result = [x1, y1]
	return result

def determine(eq1, eq2):
	a1 = float(eq1[0])
	b1 = float(eq1[1])
	c1 = float(eq1[2])

	a2 = float(eq2[0])
	b2 = float(eq2[1])
	c2 = float(eq2[2])

	dx = (c1 * b2) - (c2 * b1)
	dy = (a1 * c2) - (a2 * c1)
	D = (a1 * b2) - (a2 * b1)
	x = dx / D
	y = dy / D
	result = [x,y]
	return result

def composers(vectorA, vectorB, vectorC):
	# Finds the composers of vectorA, by using directions of B and C

	# Arrange the vectors as two equations with two variables
	eq1 = [float(vectorB[0]), float(vectorC[0]), float(vectorA[0])]
	eq2 = [float(vectorB[1]), float(vectorC[1]), float(vectorA[1])]

	# Determine S and t by solving the above equations
	equate = determine(eq1, eq2)

	S = equate[0]
	t = equate[1]

	print("S = " + str(S))
	print("t = " + str(t))
	print("composers for A:\n")

	# Calculate the composing vectors
	compb = productWithNumber(S, vectorB)
	compc = productWithNumber(t, vectorC)
	result = [compb, compc]
	print("Vector B:\n" + str(compb) + "\n")
	print("Vector C:\n" + str(compc) + "\n")

	return result

def project(vectorA, vectorB, draw=0):
	length = (scalarProduct(vectorA,vectorB))/(findLength(vectorB))
	print("Length of projection: " + str(length))
	unitB = findUnitVector(vectorB)
	result = productWithNumber(length, unitB)
	print("Projection: " + str(result))

	if draw == 1:
		drawListOfVectors([vectorA, vectorB, result])

	return result

def findUnitVector(vector):
	#Find the length of the vector
	length = findLength(vector)

	#Divide 1 with the length
	number = 1 / length

	# Calculate the x and y of the new vector (round to three decimals)
	x = "%.3f" % (number * vector[0])
	y = "%.3f" % (number * vector[1])

	#Set the result in a list
	result = [x,y]

	return result

def findAngle(vectorA, vectorB):
	# Find the scalar product
	scalar = scalarProduct(vectorA,vectorB)

	#Find the length of the vectors
	lengthA = math.sqrt(vectorA[0]**2 + vectorA[1]**2)
	lengthB = math.sqrt(vectorB[0]**2 + vectorB[1]**2)

	# Find cosinus for the angle
	cosinus = (scalar)/(lengthA*lengthB)

	#Inverse cosinus
	result = math.degrees(math.acos(cosinus))
	#print("Angle: " + str("%.3f" %result))
	return result

def twoItAll(vectorA, vectorB):
	print("Vector A:" + "\n")
	print("Unit of VectorA: " + str(findUnitVector(vectorA)))
	print("Length of vectorA: " + str(findLength(vectorA)) + "\n")
	

	print("VectorB: " + "\n")
	print("Unit of VectorB: " + str(findUnitVector(vectorB)))
	print("Length of vectorB: " + str(findLength(vectorB)) + "\n")

	print("Both vectors: " + "\n")
	print("Scalar product of vectors: " + str(scalarProduct(vectorA, vectorB)))
	print("Angle between vectors: " + str(findAngle(vectorA,vectorB)))

	#Draw them
	drawTwoVectorsWithAngle(vectorA,vectorB)

#drawVector([4,5])
#drawListOfVectors([[-2,2],[3,-5],[6, 1.5]])
#drawListOfVectorsConnected([[-3,2],[5,3],[-2,-5]])
#drawTwoVectorsWithAngle([4,5],[1,4])
#print(str(findUnitVector([3,6])))
#print(str(findAngle([-3,2], [5,3])))
#print("Product: "+ str(scalarProduct([-13,21], [8,5])))
twoItAll([4,2],[-2,-3])
#print(str(scalarProduct([2,3], [1,4]) + scalarProduct([2,3], [3,5])))
#composers([14,-3], [2,1], [4,-3])
#project([1,4],[4,5], 1)
#solveEquation([-3, 2],["x", 2], 115)
