import sys
import math
import coor

def findExpoA(pointA, pointB):
	x1 = float(pointA[0])
	x2 = float(pointB[0])
	y1 = float(pointA[1])
	y2 = float(pointB[1])
	divideY = y2 / y1
	deltaX = x2 - x1
	result = math.pow(divideY, 1.0/deltaX)
	print(str("a = " + str(result)))
	return result

def findExpoB(point, a):
	x = float(point[0])
	y = float(point[1])
	a = float(a)

	result = (y)/(a**x)
	print("b = " + str(result))
	return result

def findExpoEquation(pointA,pointB):
	a = findExpoA(pointA,pointB)
	b = findExpoB(pointA, a)
	result = "y = " + str(b) + " * " + str(a) + "^x"
	print(result)
	return result

findExpoEquation([0,230],[3,74])