import sys
import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def distance(pointA, pointB):
	x1 = float(pointA[0])
	y1 = float(pointA[1])
	x2 = float(pointB[0])
	y2 = float(pointB[1])

	result = math.sqrt((x2-x1)**2 + (y2-y1)**2)
	print("Distance: " + str(result))
	findSlope(pointA,pointB)
	return result


def plotLineBetweenPoints(pointA,pointB):
	all_data = [pointA,pointB]
	x = []
	y = []
	for point in all_data:
	    for point2 in all_data:
	        plt.plot([point[0], point2[0]], [point[1], point2[1]])
	plt.scatter(x,y)
	plt.grid()
	plt.show()

def findSlope(pointA, pointB):
	x1 = float(pointA[0])
	y1 = float(pointA[1])
	x2 = float(pointB[0])
	y2 = float(pointB[1])
	result = (y2-y1)/(x2-x1)
	return result

def findYIntercept(pointA, pointB):
	x1 = float(pointA[0])
	y1 = float(pointA[1])
	x2 = float(pointB[0])
	y2 = float(pointB[1])
	slope = findSlope(pointA, pointB)
	result = y1 - (slope*x1)
	return result

def findYInterceptWithSlope(slope,point):
	x1 = float(point[0])
	y1 = float(point[1])
	result = y1 - (slope*x1)
	return result

def determineEquation(pointA, pointB):
	x1 = float(pointA[0])
	y1 = float(pointA[1])
	x2 = float(pointB[0])
	y2 = float(pointB[1])
	slope = findSlope(pointA, pointB)
	b = findYIntercept(pointA,pointB)
	resultString = ""
	if b > 0:
		resultString = "Equation: y = " + str(slope) + "x + " + str(b)
	elif b == 0:
		resultString = "Equation: y = " + str(slope) +"x"
	else:
		resultString = "Equation: y = " + str(slope) + "x - " + str(-b)
	print(resultString)
	return resultString

def findLineAngleWithSlope(a):
	if a < 0:
		a = -a
	v = math.degrees(math.atan(a))
	print ("Angle: " + str(v))
	return v

def findAngleIntercept(line1a, line2a):
	angle1 = findLineAngleWithSlope(line1a)
	angle2 = findLineAngleWithSlope(line2a)
	result = 0
	if line1a < 0 and line2a > 0:
		result =  angle1+angle2
	elif line1a > 0 and line2a < 0:
		result =  angle1+angle2
	else:
		if line1a > line2a:
			result =  angle1-angle2
		else:
			result =  angle2-angle1


	if result < 0:
		result = -result
	print ("Result: " + str(result))
	return result

def distanceFromPointToLine(point, line):
	a = line[0]
	b = line[1]
	x = point[0]
	y = point[1]
	number = a * x + b - y

	if number < 0:
		number = -number

	result = (number)/(math.sqrt(a**2 + 1))
	print("Distance: " + str(result))
	return result


def pointInMiddleOfLineBetweenPoints(pointA,pointB):
	x1 = float(pointA[0])
	y1 = float(pointA[1])
	x2 = float(pointB[0])
	y2 = float(pointB[1])

	x = (x1+x2)/(2)
	y = (y1+y2)/(2)
	print("Middle: (" + str(x) + ";" + str(y) + ")")
	return [x,y]

def circleXYIntercept(center, r):
	a = center[0]
	b = center[1]
	x1 = a - math.sqrt(r**2 - b**2)
	x2 = a + math.sqrt(r**2 - b**2)

	y1 = b - math.sqrt(r**2 - a**2)
	y2 = b + math.sqrt(r**2 - a**2)
	x = [[x1, 0.0], [x2, 0.0]]
	y = [[0.0, y1], [0.0,y2]]
	print("x intercepts: " + str(x))
	print("y intercepts: " + str(y))

	return [x,y]

def projectPointToLine(point, line):
	a = line[0]
	b = line[1]

	pa = -(a**-1)
	pb = findYInterceptWithSlope(pa, point)

	if pa * line[0] != -1:
		print("Error!")

	resultString = ""
	if pb > 0:
		resultString = "Equation: y = " + str(pa) + "x + " + str(pb)
	else:
		resultString = "Equation: y = " + str(pa) + "x - " + str(-pb)
	print(resultString)
	return resultString

def determine(eq1, eq2):
	a1 = float(eq1[0])
	b1 = float(eq1[1])
	c1 = float(eq1[2])

	a2 = float(eq2[0])
	b2 = float(eq2[1])
	c2 = float(eq2[2])

	dx = (c1 * b2) - (c2 * b1)
	dy = (a1 * c2) - (a2 * c1)
	D = (a1 * b2) - (a2 * b1)
	x = dx / D
	y = dy / D
	result = [x,y]
	print(str(result))
	return result

#distance([-2,-3], [2,2])
determineEquation([-2.5,2], [5,0.5])
#plot([0,0], [5,-1])
#findLineAngleWithSlope(2)
#print(str(findYInterceptWithSlope(-0.65,[24.94,49.98])))
#findAngleIntercept(2, -1)
#circleXYIntercept([3,-2], 5)
#findAngleIntercept(2,-1)
#pointInMiddleOfLineBetweenPoints([-2,-1], [4,2])
#distanceFromPointToLine([-5,-2], [0.4,2])
#distance([1,6], [4.12,2.1])
#projectPointToLine([-5,-4],[-1.2, -3])
#determine([-0.37,-1,-3.5],[0.56,-1,7])
