import sys
import math

def ball(r):
    r = float(r)
    # 4/3 * pi * r^3
    answer = (4.0/3.0) * math.pi * (r*r*r)
    return answer

def ballArea(r):
    A = 4 * math.pi * r**2
    print("Ball area: " + str(A))
    return A

def testBall():
    try:
        r = float(raw_input("Radius: "))
    except ValueError:
        print("You have to enter a number")
    print(str(ball(r)))

#testBall()

def cone(r, h):
    r = float(r)
    h = float(h)
    # 1/3 * pi * h * r^2
    answer = (1.0/3.0) * math.pi * h * (r*r)
    return answer

def pyramid(b, h):
    b = float(b)
    h = float(h)
    # 1/3 * base area * height
    answer = (1.0/3.0) * b * h
    return answer

def flatPyramid(h, bigB, smallB):
    h = float(h)
    bigB = float(bigB)
    smallB = float(smallB)
    if bigB > smallB:
        answer = (h/3.0) * ((bigB + smallB) + math.sqrt(bigB * smallB))
        return answer
    else:
        return None
def diagonalInBox(length, width, height):
    d = math.sqrt(height**2 + width**2 + length**2)
    print("Diagonal: " + str(d))
    return d

def prism(G, h):
    V = G * h
    print("Prism volume: " + str(V))
    return V

def rightTrianglePrism(side, height):
    side = float(side)
    height = float(height)
    v = math.radians(60.0)

    G = (side/2.0) * side * math.sin(v)

    V = G * height
    print("Volume of prism: " + str(V))
    return V

def cylinderCurlyArea(r,h):
    T = 2 * math.pi * r * h
    print("Cylinder curly area: " + str(T))

#diagonalInBox(5,5,3)
rightTrianglePrism(26,45)
#cylinderCurlyArea(5,10)
#diagonalInBox(2.4,1.9,1.4)
