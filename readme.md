#Math equations and calculations
This is a collection of scripts containing equations and other kinds of calculations and plotting, boiled down to be almost fully automatic.

The scripts were written for my own use, and are designed as such. Don't expect it them to give the best user-experience (sorry).

##Requirements
Some of the scripts have some basic requirements. It is recommended to install them no matter what. All issues can be avoided by doing so. 

###Windows
Python 2.7.x - Can be installed from [here][1]      
[Setuptools][2] and [pip][3] - for installing requirements

When the above has been installed you can install the rest by going to the source directory and running:

    pip install -r requirements.txt

###Mac
On OS X Python is installed OOTB. You can then use [this][4] script to quickly setup Python. When in the same directory as the script run:

    sudo sh ready_python.sh

When the script is done run the following command from the source directory:

    pip install -r requirements.txt

###Linux
You can use the script mentioned above to quickly setup Python. When in the same directory as the script run:

    sudo sh ready_python.sh

When the script is done run the following command from the source directory:

    pip install -r requirements.txt

## License
Copyright (C) 2014 Mathias Jensen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


  [1]: http://www.python.org/ftp/python/2.7.6/python-2.7.6.msi "Python windows installer"
  [2]: http://www.lfd.uci.edu/~gohlke/pythonlibs/5mgns8b7/setuptools-2.0.2.win32-py2.7.exe "Setuptools windows"
  [3]: http://www.lfd.uci.edu/~gohlke/pythonlibs/5mgns8b7/pip-1.5.win32-py2.7.exe "pip Windows installer"
  [4]: https://gist.github.com/mj3052/7386186/download "Quick setup script"