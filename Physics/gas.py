#Physics using the molar gas constant

import sys
import math

#Gas constant with 8 decimals
R = 8.3144621

"""def findnWithMassAndAtomicmass(V=None,p=None,T=None,m=None,A=None):
    if m == None and A == None:
        findn(V,p,T)
    elif m == None or A == None:
        print("Error")
    else:
        n = A / m
    return n"""

def findT(n,V,p):
    top = p * V
    bottom = n * R
    result = top / bottom
    return result

def findn(V,p,T):
    top = p * V
    bottom = R * T
    result = top / bottom
    return result

def findV(n,p,T):
    top = n * R * T
    result = top / p
    return result

def findp(n,V,T):
    top = n * R * T
    result = top / V
    return result

def findRho(M,p,T):
    result = (M * p) / (R * T)
    return result

def idealGasEquation(n=None,V=None,p=None,T=None): 
    # Create a list from the given variables
    variables = [n,V,p,T]
    
    # Set the count of how many variables are None to 0
    equalNone = 0
    
    # Count how many variables equals to None
    for x in range(len(variables)):
        if variables[x] == None:
            equalNone = equalNone+1
    
    # Set the default answer, this will be changed if solution is found.
    answer = "No answer found"
    result = 0
    # If more than one variable is equal to None, we can't find any solutions :(
    if equalNone > 1:
        print("Error, did you enter all variables?")
    
    # If that isn't the case, check which variable is missing and calculate it.
    elif equalNone == 1:
        if T == None:
            answer = "T = " + str(findT(n,V,p)) + " K"
            result = ["p", findT(n,V,p)]
        elif n == None:
            answer = "n = " + str(findn(V,p,T)) + " mol"
            result = ["p", findn(V,p,T)]
        elif V == None:
            answer = "V = " + str(findV(n,p,T)) + " m3" + "\n" + "V = " + str(findV(n,p,T)*1000) +" l"
            result = ["p", findV(n,p,T)]
        elif p == None:
            answer = "p = " + str(findp(n,V,T)) + " Pa"
            result = ["p", findp(n,V,T)]
            
    # Print the answer
    print answer
    return result

# n: mol; 
# V: m3; 
# T: K; 
# P: Pa
#var = idealGasEquation(V=1, p=101325 ,T=293.15, n=None)

#print("Weight: " + str(var[1] * 29) + " g")

# It's ALIVE!
#print(str(findRho(29, 100000, 283.15)) + " g")
#print("Mass: " + str(findRho(29, 100000, 283.15)* 1236))
#print(str(findRho(29, 100000, 353.15)) + " g")
#print("Mass: " + str(findRho(29, 100000, 353.15)* 1236))
#print(str(findRho(29, 101325, 283.15)) + " g")

#print(str(findRho(29, 101325, 353.15)) + " g")
idealGasEquation(p=111800, T=273.15+22, V=0.00437)
#idealGasEquation(V=30, n=209.283825076, p=10132.5)
#idealGasEquation(n=209.283825076, p=10132.5, V=30)


